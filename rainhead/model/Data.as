package rainhead.model 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Mc
	 */
	public class Data
	{		
		//Screen config
		public static const screenWidth:int = 640;
		public static const screenHeight:int = 480;
		public static const screenColor:uint = 0x00000000;
		//Bitmap config
		public static const pixelRegions:int = 4;
		public static const pixelsWidth:int = 5000;
		public static const pixelsHeight:int = 5000;
		private static const count:int = 100000;
		//Positioning const
		private const marginX:int = (pixelsWidth - (screenWidth / 2)) / 2;
		private const marginY:int = (pixelsHeight - (screenHeight / 2)) / 2;
		
		//Source files
		[Embed(source = "../../../assets/snowflakes-a.png")]
		private static const snowAlpha:Class;
		[Embed(source = "../../../assets/snowflakes-b.png")]
		private static const snowBlue:Class;
		[Embed(source = "../../../assets/snowflakes-g.png")]
		private static const snowGreen:Class;
		[Embed(source = "../../../assets/snowflakes-y.png")]
		private static const snowYellow:Class;
		
		//Pixel vars
		private var pixelsVector:Vector.<BitmapData>;
		private var pixels:Vector.<BitmapData>;
		//Speed vars
		private const ms:int = 1000;
		private var vx:int = 20;
		private var vy:int = 30;
		private var _positions:Vector.<Point>;
		
		public function Data():void {
			initPixels();
			initRegions();
			fillPixels();
			initPositions();
		}
		
		private function initPixels():void 
		{
			pixelsVector = new Vector.<BitmapData>();
			pixelsVector.push((new snowBlue() as Bitmap).bitmapData);
			pixelsVector.push((new snowGreen() as Bitmap).bitmapData);
			pixelsVector.push((new snowYellow() as Bitmap).bitmapData);
		}
		
		private function initRegions():void 
		{
			pixels = new Vector.<BitmapData>();
			for (var i:int = 0; i < pixelRegions; i++) 
			{
				pixels.push(new BitmapData(pixelsWidth, pixelsHeight, true, screenColor));
			}
		}
		
		private function fillPixels():void 
		{
			//Setup alpha
			var alpha:BitmapData = (new snowAlpha() as Bitmap).bitmapData;
			var alphaWidth:int = alpha.width;
			var alphaHeight:int = alpha.height;
			//Setup vector
			var length:int = pixelsVector.length;
			var r:Rectangle = new Rectangle(0, 0, 32, 32);
			var p:Point = new Point();
			var a:Point = new Point();
			//Fill vector
			for (var i:int = 0; i < count; i++) 
			{
				p.x = Math.random() * (pixelsWidth - alphaWidth);
				p.y = Math.random() * (pixelsHeight - alphaHeight);
				(pixels[int(Math.random() * pixelRegions)] as BitmapData).copyPixels(pixelsVector[int(Math.random() * length)], r, p, alpha, a, true); 
			}
		}
		
		private function initPositions():void 
		{
			_positions = new Vector.<Point>();
			_positions.push(new Point( -pixelsWidth, -pixelsHeight));
			_positions.push(new Point( 0, -pixelsHeight));
			_positions.push(new Point( -pixelsWidth, 0));
			_positions.push(new Point( 0, 0));
		}
		
		public function getPixels():Vector.<BitmapData>
		{
			return pixels;
		}
		
		public function increaseX():void 
		{
			vx += 10;
		}
		
		public function decreaseX():void 
		{
			vx -= 10;
		}
		
		public function increaseY():void 
		{
			vy += 10;
		}
		
		public function decreaseY():void 
		{
			vy -= 10;
		}
		
		public function updateModel(diff:int):void 
		{
			//Update animation
			for each (var p:Point in _positions) 
			{
				p.x += vx * diff / ms;
				if (p.x < (-pixelsWidth - marginX)) {
					p.x += 2 * pixelsWidth;
				}
				else if (p.x > (pixelsWidth - marginX)) {
					p.x -= 2 * pixelsWidth;
				}
				p.y += vy * diff / ms;
				if (p.y < (-pixelsHeight - marginY)) {
					p.y += 2 * pixelsHeight;
				}
				else if (p.y > (pixelsHeight - marginY)) {
					p.y -= 2 * pixelsHeight;
				}
			}
		}
		
		public function get positions():Vector.<Point> 
		{
			return _positions;
		}
	}
}