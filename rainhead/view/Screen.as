package rainhead.view 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import rainhead.model.Data;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Screen extends Sprite 
	{	
		private var data:Data;
		private var snowquads:Vector.<Bitmap>;
		private var positions:Vector.<Point>;
		
		public function Screen(data:Data) 
		{
			super();
			this.data = data;
			var pixels:Vector.<BitmapData> = data.getPixels();
			snowquads = new Vector.<Bitmap>();
			for each (var src:BitmapData in pixels) 
			{
				var b:Bitmap = new Bitmap(src);
				snowquads.push(b);
				addChild(b);
			}
		}
		
		public function updateScreen():void 
		{
			positions = data.positions;
			for (var i:int = 0; i < Data.pixelRegions; i++) 
			{
				(snowquads[i] as Bitmap).x = (positions[i] as Point).x;
				(snowquads[i] as Bitmap).y = (positions[i] as Point).y;
			}	
		}
	}
}